<?php
/**
 * Created by PhpStorm.
 * User: cameronsharp
 * Date: 05/06/2016
 * Time: 14:28
 */

$host = '127.0.0.1'; // Change these to suit the server (Must be an admin user)
$user = 'root';
$pass = 'root';

$newuser = 'marketforce'; // These are for the new user that we will create for the application.
$newpass = 'Password1'; // There's no need to give the application user admin privileges

$dbcon = new mysqli($host, $user, $pass); // Connect to DB
if ($dbcon->connect_errno) {
    echo "Error connecting to MySQL: (" . $dbcon->connect_errno . ") " . $dbcon->connect_error . PHP_EOL;
    die; // No need to run the rest of the code if we don't have a connection
} else {
    echo "Successfully connected to MySQL" . PHP_EOL;
}

if (!$dbcon->query("CREATE DATABASE IF NOT EXISTS marketforce")) { // Create the DB if it doesn't exist
    echo "Failed creating new database" . PHP_EOL;
    echo $dbcon->error . PHP_EOL; // Print error
    die;
} else {
    echo "Successfully created new database" . PHP_EOL; // Give nice output to user
}

$dbcon->select_db("marketforce"); // Select the DB
echo "Selected DB marketforce" . PHP_EOL;

// Create table if it doesnt exist, one thing to notice is the percent column.
// (we'll use this later to store the value from the formula in the brief)
$table_create = "CREATE TABLE IF NOT EXISTS scores 
(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
job_id INT NOT NULL, 
location_name VARCHAR(10) NOT NULL, 
location_id INT NOT NULL,
area_name VARCHAR(10) NOT NULL, 
area_id INT NOT NULL, 
month_visited INT NOT NULL, 
points_scored INT NOT NULL, 
points_out_of INT NOT NULL, 
percent INT NOT NULL
)";

if (!$dbcon->query($table_create)) { // Run the above query, die if errors occur
    echo "Failed creating new table" . PHP_EOL;
    echo $dbcon->error . PHP_EOL; // Print error
    die;
} else {
    echo "Successfully created new table" . PHP_EOL; // Give nice output to user
}

// Create user from variables at the top
// There's a lot going on here, this is how the query looks without putting the vars in:

// CREATE USER 'user'@'host' INDENTIFIED BY 'pass';
// GRANT ALL PRIVILEGES ON 'marketforce'.* TO 'user'@'localhost';
// FLUSH PRIVILEGES;
$user_create = "CREATE USER '" . $newuser . "'@'" . $host . "' IDENTIFIED BY '" . $newpass . "'; 
GRANT ALL PRIVILEGES ON marketforce.* TO '" . $newuser . "'@'" . $host . "'; 
FLUSH PRIVILEGES;";

// Execute the query
if (!$dbcon->multi_query($user_create)) { // Had a few issues before switching to 'multi_query'
    echo "Failed creating new user" . PHP_EOL;
    echo $dbcon->error . PHP_EOL; // Print error
    die;
} else {
    echo "Successfully created new user: " . $newuser . PHP_EOL; // Give nice output to user
}

$dbcon->close(); // Close the DB connection

echo "Finished!" // Done!

?>
