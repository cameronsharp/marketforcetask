<?php
/**
 * Created by PhpStorm.
 * User: cameronsharp
 * Date: 05/06/2016
 * Time: 23:38
 */

// This file takes a csv with the following headers and inputs it into the 'scores' table.
// It then computes the percentage score and also stores that into the database.
// JobID	LocationName	LocationID	Area	AreaID	Month	PointsScored	PointsOutOf

require_once('mysql_connect.php'); // Connect to DB

$argv = $_SERVER['argv'];

if($argv[1]) {
    $file = $argv[1]; // Check for filename in command line arguments
} else {
    echo "Please provide a file name" . PHP_EOL;
    die; // Dies if filename not found
}

$file = $_SERVER['PWD'] . '/' . $file; // Sets the full path to the csv file

// Load data into MySQL, ignoring the first line which has the column names
$load_data = "LOAD DATA LOCAL INFILE '$file' INTO TABLE scores FIELDS TERMINATED BY ',' IGNORE 1 LINES (
job_id,
location_name,
location_id,
area_name,
area_id,
month_visited,
points_scored,
points_out_of);";

if (!$dbcon->query($load_data)) { // Load data into DB
    echo "Failed loading data" . PHP_EOL;
    echo $dbcon->error . PHP_EOL; // Print error
    die;
} else {
    echo "Successfully loaded data into database" . PHP_EOL; // Give nice output to user
}

// Pull the data back out of the DB to manipulate
$data = $dbcon->query("SELECT * FROM scores");

while($row = mysqli_fetch_array($data)){
    if ($row['points_scored'] == 0) { // Whenever 0 scored points, default to 0% so we don't trigger warnings
        $percent = 0;
    } else {
        // Formula given in brief (rounded to one decimal point)
        $percent = round(($row['points_scored'] * 100) / $row['points_out_of'], 1);
    }
    // Also, I'm going to clean up the location name and the area name to just the letter
    // by splitting the string at the space
    $new_location_name = explode(' ', $row['location_name'])[1];
    $new_area_name = explode(' ', $row['area_name'])[1];

    // Put the modified data back into the DB
    $update = "UPDATE scores SET 
    percent='" . $percent . "',
    location_name='" . $new_location_name . "',
    area_name='" . $new_area_name . "'
    WHERE id=" . $row['ID'] . ";";

    if (!$dbcon->query($update)) { // Load data into DB
        echo "Failed updating data" . PHP_EOL;
        echo $dbcon->error . PHP_EOL; // Print error
        die;
    } else {
        echo "Successfully loaded updated data into database" . PHP_EOL; // Give nice output to user
    }
    
}
