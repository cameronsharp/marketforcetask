<?php
/**
 * Created by PhpStorm.
 * User: cameronsharp
 * Date: 06/06/2016
 * Time: 12:54
 */

require_once('mysql_connect.php'); // Connect to DB

header('Content-type: application/json'); // Set JSON header because whatever happens, we're going to return JSON

if ($_SERVER['REQUEST_METHOD'] != 'GET') { // We only want to accept GET requests
    http_response_code(405); // Respond and die with http response 405 (Method not allowed)
    die;
}

$endpoint = explode("/", trim($_SERVER['PATH_INFO'], "/"))[0]; // Set endpoint (http://localhost/api/{endpoint})

// Set subendpoint (http://localhost/api/endpoint/{}). Escaped because we query the DB with the string later.
$subendpoint = $dbcon->real_escape_string(explode("/", trim($_SERVER['PATH_INFO'], "/"))[1]);

if ($endpoint == ''){ // Check if we're given an endpoint
    $data = array('error' => 'No endpoint given'); // Set error
    echo json_encode($data); // Return JSON
    die;
}

// These endpoints are allowed
$endpoints = array('overall', 'location', 'area', 'list');
// These endpoints need a subendpoint aswell
$endpoint_allowed_sub = array('location', 'area');
/*
 * The endpoints do the following:
 * overall: This returns data for the overall chart on the index page
 * location: This returns data for the individual location graphs. Requires a sub endpoint eg. /api/location/{}
 * area: This returns data for the area graphs. Requires a sub endpoint eg. /api/area/{}
 * list: This returns a list of all the locations and areas. eg. {'location': 'A', 'area': 'A'}
 */

if (!in_array($endpoint, $endpoints)) { // Check if the endpoint is in the allowed endpoints array
    $data = array('error' => 'Endpoint invalid'); // Set error
    echo json_encode($data); // Return JSON
    die;
} elseif ((!in_array($endpoint, $endpoint_allowed_sub)) && ($subendpoint != '')) { // If not allowed sub endpoint and one is given)
    $data = array('error' => 'Sub endpoint given but not allowed'); // Set error
    echo json_encode($data); // Return JSON
    die;
} elseif ((in_array($endpoint, $endpoint_allowed_sub)) && ($subendpoint == '')) { // If allowed sub endpoint and one isn't given)
    $data = array('error' => 'Sub endpoint not given, but needed'); // Set error
    echo json_encode($data); // Return JSON
    die;
} else {
    switch ($endpoint) {
        case 'overall':
            $overall_averages = array( // Array to hold the average score for each month
                1 => array(0, 0), // The embedded arrays hold the total percentage and the number of scores
                2 => array(0, 0),
                3 => array(0, 0),
                4 => array(0, 0),
                5 => array(0, 0),
                6 => array(0, 0),
                7 => array(0, 0),
                8 => array(0, 0),
                9 => array(0, 0),
                10 => array(0, 0),
                11 => array(0, 0),
                12 => array(0, 0)
            );
            $overall_result = $dbcon->query("SELECT * FROM scores ORDER BY month_visited"); // Query DB
            while ($row = mysqli_fetch_array($overall_result)) { // Loop over data
                $overall_averages[$row['month_visited']][0] += $row['percent']; // Add score to array
                $overall_averages[$row['month_visited']][1]++; // Add one to number of scores
            }
            foreach ($overall_averages as $key => $value) { // Loop over array
                if ($value[1] == 0) { // If no scores, set value as "nodata"
                    $overall_averages[$key] = "nodata";
                } else { // Otherwise calculate average by dividing score by number of scores
                    $overall_averages[$key] = round(($value[0] / $value[1]));
                }
            }
            echo json_encode($overall_averages); // Send data and die
            die;
        case 'location':
            $location_scores = array( // Array to hold the score for each month default is null so we can tell later
                1 => null,            // if we have data or not
                2 => null,
                3 => null,
                4 => null,
                5 => null,
                6 => null,
                7 => null,
                8 => null,
                9 => null,
                10 => null,
                11 => null,
                12 => null
            );
            $location_query = $dbcon->query("SELECT * FROM scores WHERE location_name='" . $subendpoint . "'"); // Query DB
            while ($row = mysqli_fetch_array($location_query)) { // Loop over data
                $location_scores[$row['month_visited']] += $row['percent']; // Add score to array
            }
            foreach ($location_scores as $key => $value) { // Loop over array
                if ($value===null) { // If null, set value as "nodata"
                    $location_scores[$key] = "nodata";
                } else { // Otherwise set as percent
                    $location_scores[$key] = $value;
                }
            }
            echo json_encode($location_scores); // Send data
            die;
        case 'area':
            $area_averages = array( // Array to hold the average score for each month
                1 => array(0, 0), // The embedded arrays hold the total percentage and the number of scores
                2 => array(0, 0),
                3 => array(0, 0),
                4 => array(0, 0),
                5 => array(0, 0),
                6 => array(0, 0),
                7 => array(0, 0),
                8 => array(0, 0),
                9 => array(0, 0),
                10 => array(0, 0),
                11 => array(0, 0),
                12 => array(0, 0)
            );
            $area_result = $dbcon->query("SELECT * FROM scores WHERE area_name='" . $subendpoint . "'"); // Query DB
            while ($row = mysqli_fetch_array($area_result)) { // Loop over data
                $area_averages[$row['month_visited']][0] += $row['percent']; // Add score to array
                $area_averages[$row['month_visited']][1]++; // Add one to number of scores
            }
            foreach ($area_averages as $key => $value) { // Loop over array
                if ($value[1] == 0) { // If no scores, set value as "nodata"
                    $area_averages[$key] = "nodata";
                } else { // Otherwise calculate average by dividing score by number of scores
                    $area_averages[$key] = round(($value[0] / $value[1]));
                }
            }
            echo json_encode($area_averages); // Send data and die
            die;
        case 'list':
            $locations = array(); // Define arrays to hold the data
            $areas = array();
            $location_result = $dbcon->query("SELECT * FROM scores GROUP BY location_name"); // Query DB, grouping so no duplicates
            while ($row = mysqli_fetch_array($location_result)) { // Loop through results
                array_push($locations, array($row['location_name'], $row['location_id'])); // Push into the array
            }
            $area_result = $dbcon->query("SELECT * FROM scores GROUP BY area_name"); // Query DB, grouping so no duplicates
            while ($row = mysqli_fetch_array($area_result)) { // Loop through results
                array_push($areas, array($row['area_name'], $row['area_id'])); // Push into array
            }
            $data = array('locations' => $locations, 'areas' => $areas); // Combine arrays to one object
            echo json_encode($data); // Send it and die
            die;
    }
}
?>